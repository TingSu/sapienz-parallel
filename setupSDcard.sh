#!/bin/bash

echo "- Waiting for emulator to boot"
OUT=`adb -s $1 shell getprop init.svc.bootanim` 
while [[ ${OUT:0:7}  != 'stopped' ]]; do
  OUT=`adb -s $1 shell getprop init.svc.bootanim`
  echo '   Waiting for emulator to fully boot...'
  sleep 5
done

echo "Emulator booted!"

echo "Copy all prepared files into the SDcard"
adb -s $1 push ./sdcard/1.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/2.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/3.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/4.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/5.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/6.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/7.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/8.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/9.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/10.vcf /mnt/sdcard/
adb -s $1 push ./sdcard/Troy_Wolf.vcf /mnt/sdcard/

adb -s $1 push ./sdcard/first.img /mnt/sdcard/
adb -s $1 push ./sdcard/sec.img /mnt/sdcard/

adb -s $1 push ./sdcard/hackers.pdf /mnt/sdcard/
adb -s $1 push ./sdcard/Hacking_Secrets_Revealed.pdf /mnt/sdcard/

adb -s $1 push ./sdcard/Heartbeat.mp3 /mnt/sdcard/
adb -s $1 push ./sdcard/intermission.mp3 /mnt/sdcard/
adb -s $1 push ./sdcard/mpthreetest.mp3 /mnt/sdcard/
adb -s $1 push ./sdcard/sample.3gp /mnt/sdcard/
adb -s $1 push ./sdcard/sample_iPod.m4v /mnt/sdcard/
adb -s $1 push ./sdcard/sample_mpeg4.mp4 /mnt/sdcard/
adb -s $1 push ./sdcard/sample_sorenson.mov /mnt/sdcard/

adb -s $1 push ./sdcard/wordnet-3.0-1.html.aar /mnt/sdcard/
adb -s $1 push ./sdcard/sample_3GPP.3gp.zip /mnt/sdcard/
adb -s $1 push ./sdcard/sample_iPod.m4v.zip /mnt/sdcard/
adb -s $1 push ./sdcard/sample_mpeg4.mp4.zip /mnt/sdcard/
adb -s $1 push ./sdcard/sample_sorenson.mov.zip /mnt/sdcard/
echo "finish push"

