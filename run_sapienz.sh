#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#this is the original subjects dir
APPDIR=/home/suting/proj/mobile_app_benchmarks/sapienz_remaining_apps/
#these are the subjects provided by Ting
#APPDIR=/vagrant/results/changed_app_ase2016/
RESULTDIR=/home/suting/vagrant/androtest/results/run1/sapienz_other_apps/


TOOLDIR=/home/suting/tools/sapienz



#cd $APPDIR
#for p in `ls -d */`; do
for p in `cat $DIR/a.txt`; do

  # AVD setup
  for avd in `android list avd -c`; do
    android delete avd -n $avd
  done
  killall emulator64-x86

  echo "** CREATING EMULATOR"
  echo no | android create avd -n api19_0 -t android-19 -c 1024M -b x86 --snapshot

  echo "@@@ Processing project "$p
  mkdir -p $RESULTDIR$p
  app_src_dir=$APPDIR$p

  cd $app_src_dir
  app=`ls bin/*-debug.apk`
  apkName=`basename $app`

  echo "** DUMPING INTERMEDIATE COVERAGE "
  cd $DIR
  ./dumpCoverage.sh $RESULTDIR$p emulator-5554 &> $RESULTDIR$p/icoverage.log &

  # set up sdcard
  ./setupSDcard.sh emulator-5554 &

  echo "** RUNNING Sapienz FOR" $apkName
  cd $TOOLDIR
  date1=$(date +"%s")
  timeout 3h python main.py $app_src_dir  &> $RESULTDIR$p/tool.log
  date2=$(date +"%s")
  diff=$(($date2-$date1))
  echo "Performed ripping for $(($diff / 60)) minutes and $(($diff % 60)) seconds."  

  echo "** COPY REPORTS"
  cp -r $app_src_dir/coverages $RESULTDIR$p/
  cp -r $app_src_dir/crashes $RESULTDIR$p/
  cp -r $app_src_dir/intermediate $RESULTDIR$p/

  echo "-- FINISHED Sapienz -- "
  adb -s emulator-5554 shell am broadcast -a edu.gatech.m3.emma.COLLECT_COVERAGE
  adb -s emulator-5554 pull /mnt/sdcard/coverage.ec $RESULTDIR$p/coverage.ec

  NOW=$(date +"%m-%d-%Y-%H-%M")
  echo $NOW.$p >> $RESULTDIR/status.log

  #adb kill-server
  kill -9 `ps | grep dumpCoverage.sh | awk '{print $1}'`

done
