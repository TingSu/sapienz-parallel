# Copyright (c) 2016-present, Ke Mao. All rights reserved.


import os
import sys
import time
import subprocess

# The original diff function
#def get_diff_lines(file_path_1, file_path_2):
#	with open(file_path_1, 'r') as file1:
#		with open(file_path_2, 'r') as file2:
#			diff_lines = set(file1).symmetric_difference(file2)
#
#	diff_lines.discard('\n')
#	return diff_lines

# New Implementation: check whether the two bug reports are different
def get_diff_lines(file_path_1, file_path_2, package_name):

	diff_cmd = "diff " + file_path_1 + " " + file_path_2 + " | grep ^\"<\""
	print '$%s' % diff_cmd
	p = subprocess.Popen(diff_cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	out,err = p.communicate()

	count = 0
	lines = out.split("\n")
	for line in lines:
		if "<" in line:
			line2 = line[4:].strip()
			# exclude the differences in the description of the exception, 
			# we only care about the exact stack difference w.r.t the app under test by package name
			if line2.startswith('at'):
				count = count + 1

	return count

# get the package name
def get_app_package_name(app_path , closed_source):

	apk_path = ""
	if not closed_source:
		cmd = "find " + app_path + " -name '*-debug.apk'"
		p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		out = p.communicate()[0]
		apk_path = out.strip().split('\n')[0]
	else:
		apk_path = app_path.replace("_output","") # for .apk, the outputs is under .apk_output
	print 'apk path: %s' % apk_path
	
	cmd = "aapt dump badging " + apk_path + " | grep package | awk '{print $2}' | sed s/name=//g | sed s/\\'//g"
	p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	out = p.communicate()[0]
	package_name = out.strip()
	print 'package name: %s' % package_name

	cmd = "aapt dump badging " + apk_path + " | grep package | awk '{print $4}' | sed s/versionName=//g | sed s/\\'//g"
	p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	out = p.communicate()[0]
	version_name = out.strip()
	print 'version name: %s' % version_name

	cmd = "aapt dump badging " + apk_path + " | grep package | awk '{print $3}' | sed s/versionCode=//g | sed s/\\'//g"
        p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out = p.communicate()[0]
        version_code = out.strip()
        print 'version code: %s' % version_code
 
	cmd = "ls -t " + app_path + "/intermediate/ | tail -1"
	print "cmd: %s" % cmd
	p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	out = p.communicate()[0]
	first_generated_file = app_path + "/intermediate/" + out.strip()
	print 'the first generated file by Sapienz: %s' % first_generated_file
	base_time = 0.0
	if os.path.exists(first_generated_file):
		base_time = os.path.getmtime(first_generated_file)
	print base_time

	#time.sleep(1)

	return package_name, version_name, version_code, base_time
	
	


def get_unique_bug_reports(apk_folder_path, closed_source, path):
	"""
	process one app
	:param path: */crashes/unique
	:return: number of unique bug reports, a list of bug reports path
	"""
	count = 0
	unique_bug_reports = []
	duplicate_bug_reports = {} # the dict table of bug reports that stores (bug_report_file_path, crash_occurence, found_phase)

	unique_bug_reports_path = path + "/unique"
	try:
		os.system("rm -rf " + unique_bug_reports_path)
		os.system("mkdir " + unique_bug_reports_path)
	except:
		pass

	# get the package name
	package_name, version_name, version_code, base_time = get_app_package_name(apk_folder_path , closed_source)

	for file_name in os.listdir(path):
		if file_name.startswith("bugreport."):
			file_path = path + "/" + file_name
			is_unique = True
			for existing_file_path in unique_bug_reports:
				# If one bug report has at least *TWO* different lines w.r.t all existing bug reports, this report is a new (unique) crash. 
				# Otherwise, it is duplicate
				#if len(get_diff_lines(file_path, existing_file_path)) <= 2:
				if get_diff_lines(file_path, existing_file_path, package_name) < 1:
					is_unique = False

					# rm from the folder
					#prefix = file_name.split("bugreport.")[0]
					#os.system("rm " + path + "/" + prefix + "*")
	
					stage = ""

					duplicate_bug_reports[existing_file_path][2] = duplicate_bug_reports[existing_file_path][2] + 1

					time_span = (os.path.getmtime(file_path) - base_time)/60.0
					if time_span < duplicate_bug_reports[existing_file_path][3]:
						print "update time_span: %f" % time_span
						duplicate_bug_reports[existing_file_path][3] = time_span

                                        if "init" in file_name:
                                                stage = "init"
                                                if duplicate_bug_reports[existing_file_path][1] == "optimize":
                                                        duplicate_bug_reports[existing_file_path][1] = "both"
                                        else:
                                                stage = "optimize"
                                                if duplicate_bug_reports[existing_file_path][1] == "init":
                                                        duplicate_bug_reports[existing_file_path][1] = "both"

					break

			if is_unique:
				unique_bug_reports.append(file_path)
				
				# store the bug report info.
                                stage = ""
                                if "init" in file_path:
                                        stage = "init"
                                else:
                                        stage = "optimize"
				# compute happen time in mins
				happen_time = os.path.getmtime(file_path)
				time_span = (happen_time - base_time)/60.0
				print "unique time_span: %f" % time_span
                                duplicate_bug_reports[file_path] = [file_path, stage, 1, time_span]

				count += 1
				# cp report and test suite
				prefix = file_name.split("bugreport.")[1]
				os.system("cp " + path + "/" + "*" + prefix  + " " + unique_bug_reports_path)

	print count
	print unique_bug_reports

	return package_name, version_name, version_code, count, unique_bug_reports, duplicate_bug_reports


def remove_leading_substring(original_string, prefix):
	tmp_string = original_string.strip()
	if tmp_string.startswith(prefix):
		return tmp_string[len(prefix):].strip()
	else:
		return tmp_string

def get_all_unique_bug_reports(path, bug_report_file, bug_reports_folder, apps_list_file , closed_source):
	"""
	save a set of unique bug reports under crashes/unique/
	each report comes with its test suite
	:param path: ella output path
	:returns total number of unique bug reports, a list of bug reports path
	"""

	# a set of file paths of the *.bugreport
	unique_bug_reports = []
	count = 0

	# create bug reports folder	
	os.system("mkdir -p "+ bug_reports_folder)
	folders = [line.rstrip('\n') for line in open(apps_list_file)]
	
	for folder_name in folders:

		apk_folder_path = path + "/" + folder_name
		if closed_source == True:
			apk_folder_path = apk_folder_path + "_output" # for closed-source apks, its output dir is appended with "_output"
		if not os.path.isdir(apk_folder_path):
			continue
		crashes_folder_path = apk_folder_path + "/crashes"
		if not os.path.isdir(crashes_folder_path):
			continue

		package_name, version_name, version_code, tmp_count, tmp_reports, duplicate_bug_reports = get_unique_bug_reports(apk_folder_path, closed_source, crashes_folder_path)

		f = open(bug_report_file, 'a+')
		for report in tmp_reports:
			r = duplicate_bug_reports[report]
			print 'bug file name: %s, stage: %s, occurence: %d, time: %f' % (r[0], r[1], r[2], r[3])

			# get the root exception type
			cmd = "grep 'Long Msg' " + r[0]
			p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		        out = p.communicate()[0]
        		root_exception = "\"" + out.replace("//", "").replace("\"","\'").strip() + "\""

			cmd = "grep 'Short Msg' " + r[0] + " | cut -d ':' -f 2"
			p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
			exception_type = p.communicate()[0].strip()

			# check where this exception is thrown out
			exception_source = ""
			stack_top_line = ""
			cmd = "grep -n 'Caused by' " + r[0] 
			p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
			out = p.communicate()[0].strip()
			if out == "":
				# there is no 'Caused by'
				cmd = "sed -n " + "8p " + r[0] 
				p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
				out = p.communicate()[0].strip()
				tmp = remove_leading_substring(out,"//")
				stack_top_line = remove_leading_substring(tmp, "at")
				print "stack top line: %s" % stack_top_line
				
			else:
				print "out: %s" % out 
				# get the last 'Caused by' 
				last_caused_by = out.split('\n')[-1]
				print "last_caused_by: %s" % last_caused_by
				stack_top_line_number = int(last_caused_by.split(':')[0]) + 1
				print "stack_top_line_number: %s" % stack_top_line_number

				cmd = "sed -n " + str(stack_top_line_number) + "p " + r[0]  
				p = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
				out = p.communicate()[0].strip()
				tmp = remove_leading_substring(out,"//")
				stack_top_line = remove_leading_substring(tmp, "at")
				print "stack top line: %s" % stack_top_line

			
			# check exception signaler
			if package_name in stack_top_line:
				exception_source = "app"
			elif stack_top_line.startswith("android.") or stack_top_line.startswith("java.") or stack_top_line.startswith("javax.") or stack_top_line.startswith("com.android.") or stack_top_line.startswith("dalvik."):
				exception_source = "framework"
			else:
				exception_source = "libcore/lib" # e.g., org.apache.*, org.w3c.*, org.xml.*, org.jason.*, junit.* (see Android API doc)
			print exception_source
						
			print "stack top line: %s" % stack_top_line
			f.write(folder_name + "," +  package_name  + "," + version_name + "," + version_code + "," + "sapienz" + "," 
				+ os.path.basename(r[0]) + "," + exception_type  + "," +  str(r[2]) + "," + r[1] + "," + str(r[3]) + "," + exception_source + "," + "\"" + stack_top_line + "\"" + "," + root_exception + "\n")
		f.close()
		
		# uncomment for debug
		#if tmp_count > 0:
		#	sys.exit(0)
		#time.sleep(2)
		if tmp_reports != []:
			app_bug_reports_folder = bug_reports_folder + "/" + folder_name
			os.system("mkdir -p " + app_bug_reports_folder + ";" + " cp -r " + apk_folder_path + "/crashes/unique " + 				   			app_bug_reports_folder)
			

		print 'find %d unique crashes' % tmp_count
		count += tmp_count
		unique_bug_reports.extend(tmp_reports)

	return count, unique_bug_reports


if __name__ == "__main__":
	#ella_output_path = "/home/ccy/crash_analysis/fdroid_apps/gradle_projects/"
	#bug_reports_folder = "/home/ccy/crash_analysis/fdroid_apps/sapienz_gradle_projects_unique_bugs"
	#bug_report_file = bug_reports_folder + "/sapienz_gradle_project_bug_report.csv"
	#apps_list_file = "/home/ccy/crash_analysis/fdroid_apps/fdroid_gradle_apps_list.txt"

	# for the data on the csl15 machine
	#ella_output_path = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/ant_compile_failed_apks/compile_failed_apks/"
        #bug_reports_folder = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/sapienz_ant_compile_failed_projects_unique_bugs"
        #bug_report_file = bug_reports_folder + "/sapienz_ant_compile_failed_projects_bug_report.csv"
        #apps_list_file = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/ant_compile_failed_apks/compile_failed_apks/apps_list.txt"

        #ella_output_path = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/gradle_compile_failed_apks/"
        #bug_reports_folder = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/sapienz_gradle_compile_failed_projects_unique_bugs"
        #bug_report_file = bug_reports_folder + "/sapienz_gradle_compile_failed_projects_bug_report.csv"
        #apps_list_file = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/gradle_compile_failed_apks/apps_list.txt"

        #ella_output_path = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/ant_projects/"
        #bug_reports_folder = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/sapienz_ant_projects_unique_bugs"
        #bug_report_file = bug_reports_folder + "/sapienz_ant_projects_bug_report.csv"
        #apps_list_file = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/fdroid_ant_apps_list.txt"

        #ella_output_path = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/new_gradle_projects_578/"
        #bug_reports_folder = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/sapienz_new_gradle_projects_578_unique_bugs"
        #bug_report_file = bug_reports_folder + "/sapienz_new_gradle_projects_578_unique_bugs.csv"
        #apps_list_file = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/new_gradle_projects_578/apps_list.txt"

	#ella_output_path = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/all_missed_apks/"
        #bug_reports_folder = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/all_missed_apks_unique_bugs"
        #bug_report_file = bug_reports_folder + "/sapienz_all_missed_apks_bug_report.csv"
        #apps_list_file = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/all_missed_apks/apks_list.txt"

	#ella_output_path = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/commercial-apps/all_commercial_apks/"
	#bug_reports_folder = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/commercial-apps/all_commercial_apks_unique_bugs"
	#bug_report_file = bug_reports_folder + "/sapienz_all_commercial_apks_bug_report.csv"
	#apps_list_file = "/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/commercial-apps/all_commercial_apks/all_apks_list.txt"

	# Set the path info before collecting unique bugs
	ella_output_path = "/home/ting/Projs/recdroid_journal/study-folder/study-folder/"
	bug_reports_folder = "/home/ting/Projs/recdroid_journal/study-folder/study-folder/sapienz_bug_reports/"
	bug_report_file = bug_reports_folder + "/sapienz_bug_report_android_bug_report.csv"
	apps_list_file = "/home/ting/Projs/recdroid_journal/study-folder/study-folder/android4.4_apks.txt"
	
	# True for closed-source apks; False for open-source projects
	closed_source = True

	if os.path.exists(bug_reports_folder):
		os.system("rm -rf " + bug_reports_folder)

	count, report_paths = get_all_unique_bug_reports(ella_output_path, bug_report_file, bug_reports_folder, apps_list_file, closed_source)
	print "Total:", count
	print report_paths
