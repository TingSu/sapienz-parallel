#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#this is the original subjects dir
APPDIR=/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/gradle_compile_failed_apks/
#these are the subjects provided by Ting
#APPDIR=/vagrant/results/changed_app_ase2016/
RESULTDIR=/media/suting/4d424548-072f-4e54-ac6a-2bc3d91785a0/fdroid-apps/sapienz_gradle_compile_failed_projects_results/


TOOLDIR=/home/suting/tools/sapienz-parallel
avd_name=$1
avd_serial=$2
apps_list_file_name=$3


#cd $APPDIR
#for p in `ls -d */`; do
for p in `cat $apps_list_file_name`; do

  # kill the emulator
  kill -9 `ps | grep qemu-system | awk '{print $1}'`

  #echo "** CREATING EMULATOR"
  #echo no | android create avd -n $avd_name -t android-19 -c 512M -b x86 -s WXGA800-7in

  echo "@@@ Processing project "$p
  mkdir -p $RESULTDIR$p
  app_src_dir=$APPDIR$p

  #cd $app_src_dir
  #app=`ls bin/*-debug.apk`
  #apkName=`basename $app`

  #echo "** DUMPING INTERMEDIATE COVERAGE "
  cd $DIR
  #./dumpCoverage.sh $RESULTDIR$p $avd_serial &> $RESULTDIR$p/icoverage.log &

  # set up sdcard
  ./setupSDcard.sh $avd_serial &

  echo "** RUNNING Sapienz FOR" $app_src_dir
  cd $TOOLDIR
  date1=$(date +"%s")
  timeout 3h python main.py $app_src_dir $avd_name $avd_serial &> $RESULTDIR$p/tool.log
  date2=$(date +"%s")
  diff=$(($date2-$date1))
  echo "Performed ripping for $(($diff / 60)) minutes and $(($diff % 60)) seconds."  

  #echo "-- FINISHED Sapienz -- "
  #timeout 10s adb -s $avd_serial shell am broadcast -a edu.gatech.m3.emma.COLLECT_COVERAGE
  #timeout 10s adb -s $avd_serial pull /mnt/sdcard/coverage.ec $RESULTDIR$p/coverage.ec

  NOW=$(date +"%m-%d-%Y-%H-%M")
  echo $NOW.$p >> $RESULTDIR/status.log

  #adb kill-server
  #kill -9 `ps | grep dumpCoverage.sh | awk '{print $1}'`

done
